#! /bin/bash
# \file setup_app.sh
# \brief main file.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 03:55 PM
#
# \copyright Copyright 2020 Sangsoic author
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#             http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# \fn install ()
# \brief Installs QOSVPNM inside an application VM.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
install ()
{
	if { [[ ! -d /rw/config/qosvpnm ]] || ls /rw/config/* >/dev/null 2>&1; } &&
		[ "$EUID" -eq 0 ]; then
		mkdir -v /rw/config/qosvpnm
		cp -rv main.sh Sources /rw/config/qosvpnm/
		chmod +x /rw/config/qosvpnm/main.sh
		ln -s -v /rw/config/qosvpnm/main.sh /usr/local/bin/qosvpnm
		echo QOSVPNM installed successfully !
	else
		echo error : QOSVPNM either already installed or script was not run with root priviledge. 1>&2
	fi
}

# \fn uninstall ()
# \brief Uninstalls QOSVPNM from an application VM.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
uninstall ()
{
	if rm -rv /rw/config/qosvpnm /usr/local/bin/qosvpnm; then
		echo QOSVPNM uninstalled successfully !
	else
		echo error : cannot uninstall QOSVPNM.
	fi
}

# \fn run ()
# \brief Runs the install or uninstall routine depending on the option passed in argument from the command line.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \param 1 Command line option (-i or -u).
run ()
{
	getopts "iu" option
	case "$option" in
		i)
			install
		;;
		u)
			uninstall
		;;
		*)
			echo error : invalide option. 1>&2
		;;
	esac

}

run "$@"
