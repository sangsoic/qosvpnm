#! /bin/bash
# \file menu.sh
# \brief Contains menu routines.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 03:55 PM
#
# \copyright Copyright 2020 Sangsoic author
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#             http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

source /rw/config/qosvpnm/Sources/operation.sh
source /rw/config/qosvpnm/Sources/setup.sh

# \fn menu_select_server ()
# \brief Asks user information relative to server selection.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
menu_select_server ()
{
	local input pattern
	read -p 'qosvpnm.server pattern?: ' input
	pattern="$input"
	list_server "$pattern"
	read -p 'Confirm your choice Y/n: ' input
	[[ "$input" =~ ^[yY]$ ]] && select_server "$pattern"
}


# \fn menu_list_server ()
# \brief Asks user information relative to server listing.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
menu_list_server ()
{
	local input pattern
	read -p 'qosvpnm.server pattern?: ' input
	pattern="$input"
	list_server "$pattern"
}

# \fn menu_select_profile ()
# \brief Asks user information relative to profile selection.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
menu_select_profile ()
{
	read -p 'qosvpnm.profile name?: ' input
	pattern="$input"
	select_server_from_profile "$pattern"
}

# \fn menu_list_server_from_profile ()
# \brief Asks user information relative to server listing from a profile.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
menu_list_server_from_profile ()
{
	read -p 'qosvpnm.profile name?: ' input
	pattern="$input"
	list_server_from_profile "$pattern"
}

# \fn menu_save_profile ()
# \brief Asks user information relative to profile saving.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
menu_save_profile ()
{
	read -p 'qosvpnm.profile name?: ' input
	read -p 'qosvpnm.server pattern?: ' pattern
	save_profile "$input" "$pattern"
}

# \fn menu_remove_profile ()
# \brief Asks user information relative to profile removing.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
menu_remove_profile ()
{
	read -p 'qosvpnm.profile name?: ' input
	pattern="$input"
	remove_profile "$pattern"
}

# \fn menu ()
# \brief Runs the user menu.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
menu ()
{
	local input pattern
	PS3="qosvpnm: "
	select item in \
	'- Quit' \
	'- Setup VPN' \
	'- Select server' \
	'- List server' \
	'- Deselect server' \
	'- Show selected server' \
	'- Select profile' \
	'- List server (from profile)' \
	'- Save profile' \
	'- Remove profile' \
	'- List profile' \
	'- Change VPN Credentials' \
	'- List connection history' \
	'- Remove connection history'
	do
		case "$REPLY" in
			1)
				echo Good bye !
				break
			;;
			2)
				setup_rw
			;;
			3)
				menu_select_server
			;;
			4)
				menu_list_server
			;;
			5)
				deselect_server
			;;
			6)
				display_selected_server	
			;;
			7)
				menu_select_profile
			;;
			8)
				menu_list_server_from_profile
			;;
			9)
				menu_save_profile
			;;
			10)
				menu_remove_profile
			;;
			11)
				list_profile
			;;
			12)
				create_password_file
			;;
			13)
				list_connection_history
			;;
			14)
				remove_connection_history
			;;
			*)
				echo error : invalid option -- "$REPLY"
			;;
		esac
	done
}
