#! /bin/bash
# \file setup.sh
# \brief Contains all server oriented functionalities.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 03:55 PM
#
# \copyright Copyright 2020 Sangsoic author
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#             http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# \fn configure_ovpn_file ()
# \brief Configures a .ovpn file to work with QUBES OS.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \param 1 A given .ovpn file name
configure_ovpn_file ()
{
	[[ "${1##*.}" = "ovpn" ]] && {
		sed -i '
		/^auth-user-pass.*/ d
		/^script-security 2$/ d
		/^\(up\|down\).*/ d
		/^redirect-gateway def1$/ d
		' "$1"
		for directive in '\nauth-user-pass pass.txt'\
		'script-security 2'\
		"up 'qubes-vpn-handler.sh up'"\
		"down 'qubes-vpn-handler.sh down'"\
		"redirect-gateway def1"; do
			echo -e "$directive" >> "$1"
		done
		echo "$1" file configured.
	}
}

# \fn configure_ovpn_files ()
# \brief Recursively configures .ovpn files.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \param 1 A given .ovpn file name or directory containing .ovpn files.
configure_ovpn_files ()
{
	for file in "$@"; do
		if [[ -f "$file" ]]; then
			configure_ovpn_file "$file"
		elif [[ -d "$file" ]]; then
			echo it is a directory yikes !!
			configure_ovpn_files "$file"/*
		fi
	done
}

# \fn create_password_file ()
# \brief Setup VPN credentials.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
create_password_file ()
{
	local username password
	echo == VPN CREDENTIALS ==
	read -s -p 'username: ' username
	echo
	read -s -p 'password: ' password
	echo
	echo == END ==
	echo -n -e "$username\n$password\n" > /rw/config/vpn/pass.txt
	echo VPN credential file created.
}

# \fn  create_files ()
# \brief Creates file relative to VPN setup.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
create_files ()
{
	mkdir -v /rw/config/vpn
	mkdir -v /rw/config/vpn/{servers,profiles}
	cp -v /rw/config/qosvpnm/Sources/qubes-vpn-handler.sh /rw/config/vpn/
	cp -vf /rw/config/qosvpnm/Sources/qubes-firewall-user-script /rw/config/
	cp -vf /rw/config/qosvpnm/Sources/rc.local /rw/config/
	chmod +x /rw/config/vpn/qubes-vpn-handler.sh
	chmod +x /rw/config/qubes-firewall-user-script
	chmod +x /rw/config/rc.local
	create_password_file
}

# \fn setup_rw ()
# \brief Setup VPN.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
setup_rw ()
{
	create_files
	echo 'Please, copy your .ovpn file(s) here and type exit + <enter>.'
	cd /rw/config/vpn/servers/
	bash
	cd -
	configure_ovpn_files /rw/config/vpn/servers
}
