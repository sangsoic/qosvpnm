#! /bin/bash
# \file main.sh
# \brief main file.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 03:55 PM
#
# \copyright Copyright 2020 Sangsoic author
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#             http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


source /rw/config/qosvpnm/Sources/menu.sh
source /rw/config/qosvpnm/Sources/operation.sh

# \fn display_help ()
# \brief Displays help menu.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
display_help ()
{
	echo Usage : sudo qosvpnm '[-(r|h|m)]'
	echo Manages VPN on qubes OS virtual machine.
	echo 
	echo '  -r     select a new VPN server based on priously used search pattern or profile.'
	echo '  -m     Runs the interactive user list menu'
	echo '  -h     Prints out this menu.'
}

# \fn main ()
# \brief main routine.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
main ()
{
	getopts 'rhm' option
	case $option in
		r)
			refresh_selected_server
		;;
		h)
			display_help
		;;
		m)
			menu
		;;
		*)
			display_help
		;;
	esac
}

if [ "$EUID" -ne 0 ]; then
	echo error : QOSVPNM must be run with root priviledge. 1>&2
	display_help
else
	main "$@"
fi
